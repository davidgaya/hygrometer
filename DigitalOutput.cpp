#ifndef DigitalOutput_h
#define DigitalOutput_h

#include "Arduino.h"
#include "DigitalPins.h"

class DigitalOutput {
public:
  DigitalOutput() {};

  void attach(int pin) {
    pinMode(pin, OUTPUT);
    _pin = pin;
  };
  void on() {
    digitalWrite(_pin, HIGH);
  };
  void off() {
    digitalWrite(_pin, LOW);
  };
private:
  int _pin;
};

#endif
