//Author: David Gaya (11/Jan/2016)
// YL-38 Hygrometer on pin A0
// SG90 Servo on pin 9
// external devices power control on pin 10
#include "valve.cpp"
#include "Hygrometer.cpp"
#include "DigitalOutput.cpp"

Valve valve;
Hygrometer hygrometer;
DigitalOutput power;

#define WET 80
#define DRY 30
#define LONG 3600000 //1 hour
#define SHORT 1000 //1 second
unsigned long pause;

void setup()
{
  Serial.begin(9600);
  valve.attach(D9);
  hygrometer.attach(A0);
  power.attach(D2);

  power.on();
  stop_watering();
}

void loop()
{
  power.on();
  delay(100);  //give time to TIP120 to stabilize voltage on devices
  int humid = hygrometer.read();
  Serial.println(humid);

  if (humid > WET) { stop_watering(); }
  if (humid < DRY) { start_watering(); }

  power.off();
  delay(pause);
}

void stop_watering() {
  Serial.println("Close valve");
  valve.close();
  pause = LONG;
}

void start_watering() {
  Serial.println("Open valve");
  valve.open();
  pause = SHORT;
}

