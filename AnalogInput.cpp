#ifndef AnalogInput_h
#define AnalogInput_h

#include "Arduino.h"

class AnalogInput {
public:
  AnalogInput() {};

  void attach(int pin) {
    pinMode(pin, INPUT);
    _pin = pin;
  };

  int read() {
    return analogRead(_pin);
  };
private:
  int _pin;
};

#endif
