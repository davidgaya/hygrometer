# README #

This is an Arduino Nano sketch, to control a watering system for plants.

### Components ###
* YL-38 Hygrometer on pin A0
* SG90 Servo on pin D9
* external devices power control on pin D2

### Hygrometer YL-38 

The hygrometer is composed of a probe (YL-69) and a comparator (YL-38).
It between 3mA and 8mA of power.

### Servo SG-90

The servo consumption is about 6mA when not moving and more than 100mA when moving.

### Layout
![Scheme](watering_system.jpg)