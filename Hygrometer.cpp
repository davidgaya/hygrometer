#ifndef Hygrometer_h
#define Hygrometer_h

#include "AnalogInput.cpp"

class Hygrometer : public AnalogInput {

  public:

  int read() {
    return humidity(AnalogInput::read());
  };

  private:

  // input resistivity between 1023-0
  // output humidity between 1-90 range
  int humidity(int reading) {
    return (1024 - reading)/10;
  }
};

#endif
