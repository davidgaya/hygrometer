#ifndef Valve_h
#define Valve_h

#include "Arduino.h"
#include <Servo.h>

class Valve {
  Servo servo;
  int OPEN_ANGLE;
  int CLOSE_ANGLE;

  public:
    Valve(int open = 90, int close = 0) {
      OPEN_ANGLE = open;
      CLOSE_ANGLE = close;
    }

    Valve &attach(int pin) {
      servo.attach(pin);
      return *this;
    }

    void open() {
      servo.write(OPEN_ANGLE);
      delay(400);
    };

    void close() {
      servo.write(CLOSE_ANGLE);
      delay(400);
    }

};

#endif

